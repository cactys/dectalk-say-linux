[upstream]: https://github.com/dectalk/source

# Conquest of `say`

This repository is a fork off [this repository][upstream] (where a lot of force
 pushes are going on) and aims at getting a `say` binary built that works the
 same as `say.exe` on Windows.

There are fixes here and there, but also preliminary changes in order to get
 `say` built eventually (it is 100% Windows-only as of now).

Changes to build instructions here are all work in progress.

> **Note**: The `say` binary found in
>  `samplosf/build/dtsamples/$OS_VERSION/$LANG/release/say` is **NOT** the same
>  as `say.exe`. It's essentially doing the same as it's neighboring
>  `say_demo_$LANG` file and sounds awful (compare [here](./demo/)), thus it's
>  not what we're looking for.

---

# Fonix DECtalk
These files were found off a HTTP server on [grossgang.com](http://grossgang.com/tts/dectalk%20software%20and%20manual/Ad%202.zip)

## Docker Fun
To build DECtalk, run `sudo docker-compose up` (and make sure you have Docker and docker-compose installed!)
