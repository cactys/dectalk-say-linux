# Demo

This demo means to showcase how outputs from `say` and `say_demo_$LANG` (or the
 `dtsamples` version of `say`) differ.


## Input
```
[:PHONEME ON][:DV AP 98 AS 98 B4 331 B5 304 BF 21 BR 2 F4 2924 F5 3821 HR 19 HS 93 LA 5 LX 5 NF 5 PR 208 QU 36 RI 61 SM 39 SR 27 SX 1]This is an example of a custom voice line fed to TTS. [:dial 6044058624]
```


## Output

| Windows `say.exe`  | Linux `say` / `say_demo_$LANG` (`dtstamples`)
|:------------------:|:---
| ![](./windows.wav) | ![](./linux.wav)
